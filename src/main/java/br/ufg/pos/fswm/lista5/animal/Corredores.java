package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public abstract class Corredores extends Animal {

    public Corredores(String nome, int idade) {
        super(nome, idade);
    }

    public abstract String correr();
}
