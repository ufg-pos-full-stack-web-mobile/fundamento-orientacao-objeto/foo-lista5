package br.ufg.pos.fswm.lista5.exercicio8;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public abstract class Funcionario {

    private double salario;

    public Funcionario(double salario) {
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public abstract double getComissao();

    public double calcularSalario() {
        return getSalario() + getComissao();
    }
}
