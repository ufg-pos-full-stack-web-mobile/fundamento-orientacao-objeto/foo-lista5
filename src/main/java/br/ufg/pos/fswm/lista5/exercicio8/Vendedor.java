package br.ufg.pos.fswm.lista5.exercicio8;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class Vendedor extends Funcionario {

    public Vendedor(double salario) {
        super(salario);
    }

    public double getComissao() {
        return 0;
    }
}
