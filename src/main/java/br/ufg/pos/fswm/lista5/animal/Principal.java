package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Principal {

    public static void main(String... args) {
        Animal animal1 = new Cachorro("Rex", 5);
        Animal animal2 = new Cavalo("Alazão", 10);
        Animal animal3 = new Preguica("Ligeirinha", 4);

        Animal[] animais = {animal1, animal2, animal3};

        exercicio2(animais);
        exercicio3(animais);
        exercicio4();
    }

    private static void exercicio4() {
        System.out.println("Executando o solicitado no exercicio 4");

        Animal animal1 = new Cachorro("Rex", 5);
        Animal animal2 = new Cavalo("Alazão", 10);
        Animal animal3 = new Preguica("Ligeirinha", 4);
        Animal animal4 = new Cachorro("Toto", 5);
        Animal animal5 = new Cavalo("Ponei", 10);
        Animal animal6 = new Preguica("Apressadinho", 4);
        Animal animal7 = new Cachorro("Titiu", 5);
        Animal animal8 = new Cavalo("Pegasu", 10);
        Animal animal9 = new Preguica("Bolt", 4);
        Animal animal10 = new Cavalo("Cavalo de fogo", 20);

        Zoologico zoo = new Zoologico();
        zoo.adicionarAnimal(animal1);
        zoo.adicionarAnimal(animal2);
        zoo.adicionarAnimal(animal3);
        zoo.adicionarAnimal(animal4);
        zoo.adicionarAnimal(animal5);
        zoo.adicionarAnimal(animal6);
        zoo.adicionarAnimal(animal7);
        zoo.adicionarAnimal(animal8);
        zoo.adicionarAnimal(animal9);
        zoo.adicionarAnimal(animal10);

        zoo.percorrerJaulas();

    }

    private static void exercicio3(Animal[] animais) {
        System.out.println("Executando o solicitado no exercicio 3");

        Veterinario veterinario = new Veterinario();
        for(Animal animal : animais) {
            System.out.println("Veterinario consultando: ");
            veterinario.examinar(animal);
        }

        System.out.println();
    }

    private static void exercicio2(Animal[] animais) {
        System.out.println("Executando o solicitado no exercicio 2");

        for(Animal animal : animais) {
            System.out.println(animal.emitirSom());
        }

        System.out.println();
    }
}
