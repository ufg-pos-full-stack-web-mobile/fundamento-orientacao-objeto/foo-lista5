package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Jaula {

    private Animal animal;

    public Jaula(Animal animal) {
        this.animal = animal;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
