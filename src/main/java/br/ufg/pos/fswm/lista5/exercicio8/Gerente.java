package br.ufg.pos.fswm.lista5.exercicio8;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class Gerente extends Funcionario {

    public Gerente(double salario) {
        super(salario);
    }

    public double getComissao() {
        return 1500.0;
    }
}
