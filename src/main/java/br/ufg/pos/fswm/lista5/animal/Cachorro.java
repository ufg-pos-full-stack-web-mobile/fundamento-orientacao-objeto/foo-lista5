package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Cachorro extends Corredores {

    public Cachorro(String nome, int idade) {
        super(nome, idade);
    }

    public String emitirSom() {
        return "au-au";
    }

    public String correr() {
        return "run";
    }
}
