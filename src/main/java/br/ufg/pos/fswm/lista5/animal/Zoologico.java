package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Zoologico {

    private Jaula[] jaulas = new Jaula[10];
    private int qtdAnimais = 0;

    public void adicionarAnimal(Animal animal) {
        jaulas[qtdAnimais++] = new Jaula(animal);
    }

    public void percorrerJaulas() {
        for(Jaula jaula : jaulas) {
            final Animal animal = jaula.getAnimal();
            System.out.println(animal.emitirSom());

            if(animal instanceof Corredores) {
                System.out.println(((Corredores) animal).correr());
            }
        }
    }

}
