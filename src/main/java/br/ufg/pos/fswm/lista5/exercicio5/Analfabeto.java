package br.ufg.pos.fswm.lista5.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class Analfabeto extends Funcionario {

    public Analfabeto(String nome, int codigo) {
        super(nome, codigo);
    }

    public double getRendaBasica() {
        return 1000.0;
    }
}
