package br.ufg.pos.fswm.lista5.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class EnsinoBasico extends Funcionario {

    private String escola;

    public EnsinoBasico(String nome, int codigo, String escola) {
        super(nome, codigo);
        this.escola = escola;
    }

    public String getEscola() {
        return escola;
    }

    public void setEscola(String escola) {
        this.escola = escola;
    }

    public double getRendaBasica() {
        return 1000.0 * 1.1;
    }
}
