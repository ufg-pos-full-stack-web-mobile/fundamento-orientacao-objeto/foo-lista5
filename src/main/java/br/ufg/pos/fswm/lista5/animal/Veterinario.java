package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Veterinario {

    public void examinar(Animal animal) {
        System.out.println(animal.emitirSom());
    }

}
