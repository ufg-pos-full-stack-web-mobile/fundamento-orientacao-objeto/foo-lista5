package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Cavalo extends Corredores {

    public Cavalo(String nome, int idade) {
        super(nome, idade);
    }

    public String emitirSom() {
        return "irrirrirri";
    }

    public String correr() {
        return "pocoto, pocoto";
    }
}
