package br.ufg.pos.fswm.lista5.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class Principal {

    public static final String COLEGIO = "Colegio tal";
    public static final String ESCOLA = "Escola tal";
    public static final String FUNCIONARIO_NOME = "Funcionario1";
    public static final String UNIVERSIDADE = "Universidade tal";

    public static void main(String... args) {

        Funcionario funcionario1 = new EnsinoBasico(FUNCIONARIO_NOME, 1, ESCOLA);
        Funcionario funcionario2 = new EnsinoBasico(FUNCIONARIO_NOME, 1, ESCOLA);
        Funcionario funcionario3 = new EnsinoBasico(FUNCIONARIO_NOME, 1, ESCOLA);
        Funcionario funcionario4 = new EnsinoBasico(FUNCIONARIO_NOME, 1, ESCOLA);

        Funcionario funcionario5 = new EnsinoMedio(FUNCIONARIO_NOME, 1, ESCOLA, COLEGIO);
        Funcionario funcionario6 = new EnsinoMedio(FUNCIONARIO_NOME, 1, ESCOLA, COLEGIO);
        Funcionario funcionario7 = new EnsinoMedio(FUNCIONARIO_NOME, 1, ESCOLA, COLEGIO);
        Funcionario funcionario8 = new EnsinoMedio(FUNCIONARIO_NOME, 1, ESCOLA, COLEGIO);

        Funcionario funcionario9 = new EnsinoSuperior(FUNCIONARIO_NOME, 1, ESCOLA, COLEGIO, UNIVERSIDADE);
        Funcionario funcionario10 = new EnsinoSuperior(FUNCIONARIO_NOME, 1, ESCOLA, COLEGIO, UNIVERSIDADE);

        Funcionario[] empresa = {funcionario1, funcionario2, funcionario3, funcionario4, funcionario5, funcionario6, funcionario7, funcionario8, funcionario9, funcionario10};

        double totalGeral = 0.0;
        for(Funcionario f : empresa) {
            totalGeral += f.getRendaBasica();
        }
        String mensagem = String.format("Total da folha de pagamento da empresa: R$ %.2f", totalGeral);
        System.out.println(mensagem);

        double totalBasico = 0.0;
        double totalMedio = 0.0;
        double totalSuperior = 0.0;

        for(Funcionario f : empresa) {
            if(f instanceof EnsinoSuperior) {
                totalSuperior += f.getRendaBasica();
            } else if (f instanceof EnsinoMedio) {
                totalMedio += f.getRendaBasica();
            } else {
                totalBasico += f.getRendaBasica();
            }
        }

        mensagem = String.format("Total pago para funcionarios com ensino básico: %.2f", totalBasico);
        System.out.println(mensagem);

        mensagem = String.format("Total pago para funcionarios com ensino médio: %.2f", totalMedio);
        System.out.println(mensagem);

        mensagem = String.format("Total pago para funcionarios com ensino superior: %.2f", totalSuperior);
        System.out.println(mensagem);

    }
}
