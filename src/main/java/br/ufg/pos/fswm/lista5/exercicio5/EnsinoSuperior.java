package br.ufg.pos.fswm.lista5.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class EnsinoSuperior extends EnsinoMedio {

    private String universidade;

    public EnsinoSuperior(String nome, int codigo, String escola, String colegio, String universidade) {
        super(nome, codigo, escola, colegio);
        this.universidade = universidade;
    }

    public String getUniversidade() {
        return universidade;
    }

    public void setUniversidade(String universidade) {
        this.universidade = universidade;
    }

    @Override
    public double getRendaBasica() {
        return super.getRendaBasica() * 2;
    }
}
