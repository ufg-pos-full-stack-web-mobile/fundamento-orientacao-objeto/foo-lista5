package br.ufg.pos.fswm.lista5.exercicio8;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class Supervisor extends Funcionario {

    public Supervisor(double salario) {
        super(salario);
    }

    public double getComissao() {
        return 600.0;
    }
}
