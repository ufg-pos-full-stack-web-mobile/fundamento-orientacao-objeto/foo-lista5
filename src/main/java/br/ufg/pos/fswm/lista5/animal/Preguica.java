package br.ufg.pos.fswm.lista5.animal;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 30/04/17.
 */
public class Preguica extends Animal {

    public Preguica(String nome, int idade) {
        super(nome, idade);
    }

    public String emitirSom() {
        return "zzzz";
    }

    public String subirEmArvore() {
        return "subindo devagarzinho";
    }
}
