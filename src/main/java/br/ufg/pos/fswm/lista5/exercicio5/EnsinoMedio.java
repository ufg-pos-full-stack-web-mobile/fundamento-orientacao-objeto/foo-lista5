package br.ufg.pos.fswm.lista5.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 01/05/17.
 */
public class EnsinoMedio extends EnsinoBasico {

    private String colegio;

    public EnsinoMedio(String nome, int codigo, String escola, String colegio) {
        super(nome, codigo, escola);
        this.colegio = colegio;
    }

    public String getColegio() {
        return colegio;
    }

    public void setColegio(String colegio) {
        this.colegio = colegio;
    }

    @Override
    public double getRendaBasica() {
        return super.getRendaBasica() * 1.5;
    }
}
